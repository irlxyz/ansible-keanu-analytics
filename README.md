# ansible-keanu-analytics

## Requirements

* Debian 10 host

* Postgres database pre-initialised with kealytics/dbinit.sql script

## Usage

```
- src: git@gitlab.com:guardianproject-ops/ansible-keanu-analytics
  scm: git
  version: not yet released
```

## Role variables

The vars needed are set out in defaults/main.yml and currently are in the same
format as the configuration file used by the collector. We can reshuffle the
format of these, as things like the homeserver fqdn are going to be shared
between this and the synapse instance.

The synapse instance will also need extension to create the app service
configuration, again needing the two access tokens.

## Security considerations

Communication from the synapse instance and the metrics collector happens
in the clear, across a private subnet. AWS may host the instances in different
racks, it's not clear what distance is travelled. This leaks metadata about
the user that is talking, when they talk and in what room. Message content is
leaked for non-end-to-end encrypted rooms.

Requests for IP address information, etc. happen over TLS authenticated and
encrypted connection.

The database contains a subset of the information contained in the synapse
database. It contains enough information to identify who, when and where there
are messages, but not message content. The end-to-end state of rooms is not
revealed by the analytics database.

## Other notes

Technically we should be hosting this with a WSGI web server, but as it's not
publicly exposed I do not mind cutting this corner. If we find we have
performance issues then we can revisit this.

## TODO

User agent strings are hardcoded in template
